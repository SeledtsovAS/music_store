__author__ = 'alexey'

from app import app, login_manager, db

from flask import render_template, url_for, g, redirect, request, session
from flask.ext.login import login_user, logout_user, current_user, login_required

from .models import User, Password, TypeInstrument, Card, MusicInstrument
from .models import Producer, Image, Case, Ordering, Status

from .forms import SignInForm, ChangeInstrumentForm, LoginForm, ChangeUserForm, AddInstrumentForm
from .forms import FilterForm, ChangeStatusForm

def get_type_and_url():
    query = db.session.query(TypeInstrument).all()
    return [(i, '/catalog/type/' + str(i.id) + '/page/') for i in query]
app.jinja_env.globals['get_type_and_url'] = get_type_and_url

def get_count_instruments_in_case():
    query = db.session.query(Case).filter(Case.id_user == current_user.id).all()
    count = 0
    for item in query:
        print(item.count)
        count += item.count
    return count
app.jinja_env.globals['get_count_in_case'] = get_count_instruments_in_case

@app.route('/addintoorderingcase/<int:id>/')
@login_required
def add_into_ordering_case(id):
    ordering_instrument = Ordering(
        id_user=current_user.id, id_instrument=id, id_status=1, count=1
    )
    db.session.add(ordering_instrument)

    cases = db.session.query(Case).filter(
        Case.id_user == current_user.id, Case.id_instrument == id
    ).first()
    card = db.session.query(Card).filter(Card.id_user == current_user.id).first()
    price = db.session.query(MusicInstrument).filter(MusicInstrument.id == id).first().price

    if price > card.balance:
        return "Покупка не может быть совершена из-за отсутствия средств на карте."
    card.balance -= price
    if cases and cases.count > 1:
        cases.count -= 1
        db.session.commit()
    else:
        db.session.delete(cases)
        db.session.commit()

    return redirect(session.get('prev_url', '/'))

@app.route('/addintocase/<int:id>/')
@login_required
def add_into_case(id):
    instrument_in_case = db.session.query(Case).filter(Case.id_instrument == id).first()

    instrument = db.session.query(MusicInstrument).filter(MusicInstrument.id == id).first()
    if instrument.count == 0:
        return redirect(session.get('prev_url', '/'))
    instrument.count -= 1

    if instrument_in_case is not None:
        instrument_in_case.count += 1
        db.session.commit()
    else:
        element_case = Case(id_instrument=id, id_user=current_user.id, count=1)
        try:
            db.session.add(element_case)
            db.session.commit()
        except:
            db.session.rollback()

    return redirect(session.get('prev_url', '/'))

@app.route('/fulldescription/<int:id>/')
def full_description_instrument(id):
    node = db.session.query(MusicInstrument, TypeInstrument, Producer, Image).filter(
        MusicInstrument.id_type == TypeInstrument.id, MusicInstrument.id_producer == Producer.id,
        TypeInstrument.id == MusicInstrument.id_type, Image.id_instrument == MusicInstrument.id, MusicInstrument.id == id
    ).first()
    session['prev_url'] = request.path
    return render_template('fulldescription.html', instrument=node)

@app.route('/catalog/type/<int:type>/page/', defaults={'page': 1}, methods=['GET', 'POST'])
@app.route('/catalog/type/<int:type>/page/<int:page>/')
def catalog(type, page):
    query = db.session.query(MusicInstrument, TypeInstrument, Producer, Image).filter(
        MusicInstrument.id_type == TypeInstrument.id, MusicInstrument.id_producer == Producer.id,
        TypeInstrument.id == type, Image.id_instrument == MusicInstrument.id
    )
    form = FilterForm()
    if form.validate_on_submit() and form.filterProducer.data != 'Все':
        producer = form.filterProducer.data
        query = query.filter(Producer.marka == producer).all()

    session['prev_url'] = request.path
    return render_template('catalog.html', instruments=query, form=form)

@app.route('/delete_instrument_from_case/<int:id>/')
@login_required
def delete_from_case(id):
    query = db.session.query(Case).filter(Case.id == id).first()
    instrument = db.session.query(MusicInstrument).filter(MusicInstrument.id == query.id_instrument).first()
    instrument.count += 1

    if query and query.count > 1:
        query.count -= 1
        db.session.commit()
    else:
        db.session.delete(query)
        db.session.commit()
    return redirect(session.get('prev_url', '/'))

@app.route('/current_user/case/<int:id>/')
@login_required
def current_user_case(id):
    instruments = db.session.query(Case, MusicInstrument, TypeInstrument, Producer).filter(
        Case.id_instrument == MusicInstrument.id, Case.id_user == current_user.id,
        Producer.id == MusicInstrument.id_producer, MusicInstrument.id_type == TypeInstrument.id
    ).all()

    balance = db.session.query(Card).filter(Card.id_user == current_user.id).first()
    session['prev_url'] = request.path
    return render_template('currentusercase.html', instruments=instruments, balance=balance)

def get_producers():
    return [(prod.id, prod.marka) for prod in db.session.query(Producer).all()]

def get_type_instruments():
    return [(type.id, type.type) for type in db.session.query(TypeInstrument).all()]

@app.route('/admin/add_new_instrument/', methods=['GET', 'POST'])
@login_required
def add_new_instrument():
    form = AddInstrumentForm()
    if form.validate_on_submit():
        id_type = db.session.query(TypeInstrument).filter(TypeInstrument.type == form.type.data).first().id
        id_producer = db.session.query(Producer).filter(Producer.marka == form.producer.data).first().id

        instrument = MusicInstrument(
            name=form.name.data, description=form.description.data,
            count=form.count.data, price=form.price.data, id_type=id_type, id_producer=id_producer
        )
        db.session.add(instrument)
        db.session.commit()

        instrument_id = db.session.query(MusicInstrument).filter(MusicInstrument.name == form.name.data).first().id
        image = Image(url='img/' + form.image.data.filename, id_instrument=instrument_id)
        db.session.add(image)
        db.session.commit()

        request.files['image'].save('./app/static/img/' + form.image.data.filename)
        return redirect(session.get('prev_url', '/'))

    return render_template('addnewinstrument.html', form=form)

@app.route('/admin/instrument_change/<int:id>/', methods=['GET', 'POST'])
@login_required
def change_data_instrument(id):
    form = ChangeInstrumentForm()

    instrument = db.session.query(MusicInstrument, TypeInstrument, Producer).filter(
        MusicInstrument.id_producer == Producer.id, MusicInstrument.id_type == TypeInstrument.id,
        MusicInstrument.id == id
    ).first()

    if form.validate_on_submit():
        instrument[0].name = form.name.data
        instrument[0].description = form.description.data
        instrument[0].count = form.count.data
        instrument[0].price = form.price.data
        instrument[0].id_type = db.session.query(TypeInstrument).filter(
            TypeInstrument.type == form.type.data
        ).first().id
        instrument[0].id_producer = db.session.query(Producer).filter(
            Producer.marka == form.producer.data
        ).first().id
        db.session.commit()
        return redirect(session.get('prev_url', '/'))

    form.name.data = instrument[0].name
    form.description.data = instrument[0].description
    form.count.data = instrument[0].count
    form.price.data = instrument[0].price

    return render_template('changeinstrument.html', form=form)

@app.route('/admin/instrument_delete/<int:id>/', methods=['GET', 'POST'])
@login_required
def delete_instrument(id):
    instrument = db.session.query(MusicInstrument).filter(MusicInstrument.id == id).first()
    cases = db.session.query(Case).filter(Case.id_instrument == instrument.id).first()

    try:
        db.session.delete(cases)
        db.session.commit()
    except:
        db.session.rollback()
    if instrument:
        db.session.delete(instrument)
        db.session.commit()
    return redirect(session.get('prev_url', '/'))

@app.route('/admin/user_change/<int:user_id>/', methods=['GET', 'POST'])
@login_required
def change_data_user(user_id):
    user = db.session.query(User, Card, Password).filter(
        User.id == Card.id_user, Password.id == User.id_password, User.id == user_id
    ).first()

    form = ChangeUserForm()
    if form.validate_on_submit():
        user[0].first_name = form.first_name.data
        user[0].second_name = form.second_name.data
        user[0].email = form.email.data
        user[1].balance = float(form.balance.data)
        user[2].key = form.password.data

        db.session.commit()
        return redirect(session.get('prev_url', '/'))

    if user is not None:
        form.first_name.data = user[0].first_name
        form.second_name.data = user[0].second_name
        form.balance.data = str(user[1].balance)
        form.email.data = user[0].email
        form.password.data = user[2].key

    return render_template('changeuserdata.html', form=form)

@app.route('/admin_room/users/')
@login_required
def admin_user():
    users = db.session.query(User, Card).filter(User.id == Card.id_user).all()
    session['prev_url'] = request.path
    return render_template('adminuser.html', users=users)

@app.route('/admin_room/instruments/')
@login_required
def admin_instrument():
    instruments = db.session.query(MusicInstrument, TypeInstrument, Producer).filter(
        MusicInstrument.id_producer == Producer.id, MusicInstrument.id_type == TypeInstrument.id
    ).all()

    session['prev_url'] = request.path

    return render_template('admininstrument.html', instruments=instruments)

@app.route('/admin_room/ordering/<int:id>/', methods=['GET', 'POST'])
@app.route('/admin_room/ordering/', defaults={'id': -1})
@login_required
def admin_ordering(id):
    query = db.session.query(Ordering, MusicInstrument, TypeInstrument, Producer, Status, User).filter(
        Ordering.id_instrument == MusicInstrument.id, MusicInstrument.id_type == TypeInstrument.id,
        MusicInstrument.id_producer == Producer.id, Ordering.id_status == Status.id,
        Ordering.id_user == User.id
    ).all()

    form = ChangeStatusForm()
    if form.validate_on_submit():
        order = db.session.query(Ordering).filter(Ordering.id == id).first()
        order.id_status = db.session.query(Status).filter(Status.status == form.status.data).first().id
        db.session.commit()

        if order.id_status == 5:
            db.session.delete(order)
            db.session.commit()

        return redirect(url_for('admin_ordering'))

    session['prev_url'] = request.path
    return render_template('adminorderingcase.html', orders=query, form=form, change_id=id)

@app.route('/admin_room/', methods=['GET', 'POST'])
@login_required
def admin_room():
    return render_template('adminroom.html')

@app.route('/')
@app.route('/index/')
def index():
    return render_template("index.html")

@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(user_id)


@app.route('/signin/', methods=['GET', 'POST'])
def signin():
    form = SignInForm()

    error = None
    if form.validate_on_submit():
        first_name = request.form['first_name']
        second_name = request.form['second_name']
        email = request.form['email']
        password = request.form['password']
        if not User.repeatedEmail(email):
            passwd = Password(key=password)

            db.session.add(passwd)
            db.session.commit()

            query = db.session.query(Password).filter(Password.key == password).first()
            user = User(first_name=first_name, second_name=second_name, email=email, id_password=query.id)
            db.session.add(user)
            db.session.commit()

            usr = db.session.query(User).filter(User.email == email).first()
            card = Card(balance=1000000, id_user=usr.id)
            db.session.add(card)
            db.session.commit()

            return redirect(url_for('login'))
        else:
            error = "Данный email уже привязан к сайту"

    return render_template('signin.html', form=form, error=error)


@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/login/', methods=['GET', 'POST'])
def login():
    if hasattr(g, 'user') and g.user.is_authenticated():
        return redirect(url_for('index'))

    form = LoginForm()
    error = None
    if form.validate_on_submit():
        email = request.form['email']
        password = request.form['password']
        user = db.session.query(User, Password).filter(
            User.id_password == Password.id, password == Password.key, User.email == email
        ).first()

        if user is not None:
            login_user(user[0], remember=request.form.get('remember_me'))
            return redirect(request.args.get('next') or url_for('index'))
        else:
            error = "Incorrected email or password"
    return render_template('login.html', form=form, error=error)

@app.before_request
def user_request():
    g.user = current_user