__author__ = 'alexey'

from app import db
from .models import TypeInstrument, Producer, Status

from flask.ext.wtf import Form
from wtforms import TextField, TextAreaField
from wtforms import PasswordField
from wtforms import BooleanField
from wtforms import SelectField
from wtforms import SelectMultipleField
from wtforms import IntegerField
from wtforms import DecimalField
from wtforms import FloatField
from wtforms.validators import Required, Regexp
from wtforms import PasswordField
from flask_wtf.file import FileField
from wtforms.validators import Email
from wtforms import RadioField

class LoginForm(Form):
    email = TextField('email', validators=[Required()])
    password = PasswordField('password', validators=[Required()])
    remember_me = BooleanField('remember_me', default=False)

class SignInForm(Form):
    first_name = TextField('first_name', validators=[Required()])
    second_name = TextField('second_name', validators=[Required()])
    email = TextField('email', validators=[Required()])
    password = PasswordField('password', validators=[Required()])

class ChangeUserForm(Form):
    first_name = TextField('first_name', validators=[Required()])
    second_name = TextField('second_name', validators=[Required()])
    password = TextField('password', validators=[Required()])
    email = TextField('email', validators=[Required()])
    balance = FloatField('balance', validators=[Required()])


def get_producers():
    return [(prod.marka, prod.marka) for prod in db.session.query(Producer).all()]

def get_type_instruments():
    return [(type.type, type.type) for type in db.session.query(TypeInstrument).all()]

class ChangeInstrumentForm(Form):
    name = TextField('name', validators=[Required()])
    description = TextField('description', validators=[Required()])
    count = IntegerField('count', validators=[Required()])
    price = FloatField('price', validators=[Required()])
    type = SelectField('type', choices=get_type_instruments())
    producer = SelectField('producer', choices=get_producers())

class AddInstrumentForm(Form):
    name = TextField('name', validators=[Required()])
    description = TextAreaField('description', validators=[Required()])
    count = IntegerField('count', validators=[Required()])
    price = FloatField('price', validators=[Required()])
    type = SelectField('type', choices=get_type_instruments())
    producer = SelectField('producer', choices=get_producers())
    image = FileField('image')

def get_producer_names():
    result = [('Все', 'Все')]
    result.extend([(item.marka, item.marka) for item in db.session.query(Producer).all()])
    return result

class FilterForm(Form):
    filterProducer = RadioField('filterProducer', choices=get_producer_names(), default="Все")


def get_all_status():
    return [(item.status, item.status) for item in db.session.query(Status).all()]

class ChangeStatusForm(Form):
    status = SelectField('status', choices=get_all_status())