__author__ = 'alexey'

from datetime import datetime
from app import db

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(30), index=True)
    second_name = db.Column(db.String(30), index=True)
    email = db.Column(db.String(30), index=True, unique=True)
    id_password = db.Column(db.Integer, db.ForeignKey('password.id'))
    role = db.Column(db.Integer, default=1)

    @staticmethod
    def is_authenticated():
        return True

    @staticmethod
    def is_active():
        return True

    @staticmethod
    def is_anonimous():
        return False

    def get_id(self):
        return self.id

    @staticmethod
    def repeatedEmail(email):
        query = db.session.query(User).filter(User.email == email).first()
        if query is not None:
            return True
        return False

class Password(db.Model):
    __tablename__ = 'password'
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(255), index=True)

class Card(db.Model):
    __tablename__ = 'card'
    id = db.Column(db.Integer, primary_key=True)
    balance = db.Column(db.Float)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))

class MusicInstrument(db.Model):
    __tablename__ = 'musicinstrument'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), index=True)
    description = db.Column(db.String(120), index=True)
    count = db.Column(db.Integer)
    price = db.Column(db.Float, index=True)
    id_type = db.Column(db.Integer, db.ForeignKey('typeinstrument.id'))
    id_producer = db.Column(db.Integer, db.ForeignKey('producer.id'))

class Image(db.Model):
    __tablename__ = 'image'
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(120), index=True)
    id_instrument = db.Column(db.Integer, db.ForeignKey('musicinstrument.id'))

    @staticmethod
    def is_unique(image):
        urls = db.session.query(Image).filter(Image.url == image).first()
        if urls:
            return urls
        return None

class Producer(db.Model):
    __tablename__ = 'producer'
    id = db.Column(db.Integer, primary_key=True)
    marka = db.Column(db.String(40), index=True)

class TypeInstrument(db.Model):
    __tablename__ = 'typeinstrument'
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(30), index=True, unique=True)

class Case(db.Model):
    __tablename__ = 'case'
    id = db.Column(db.Integer, primary_key=True)
    count = db.Column(db.Integer, index=True)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))
    id_instrument = db.Column(db.Integer, db.ForeignKey('musicinstrument.id'))

class Post(db.Model):
    __tablename__ = 'post'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(300), index=True)
    time = db.Column(db.DateTime, index=True, default=datetime.utcnow())
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))
    id_instrument = db.Column(db.Integer, db.ForeignKey('musicinstrument.id'))

class Ordering(db.Model):
    __tablename__ = 'ordering'
    id = db.Column(db.Integer, primary_key=True)
    count = db.Column(db.Integer)
    time = db.Column(db.DateTime, default=datetime.utcnow())
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))
    id_instrument = db.Column(db.Integer, db.ForeignKey('musicinstrument.id'))
    id_status = db.Column(db.Integer, db.ForeignKey('status.id'))

class Status(db.Model):
    __tablename__ = 'status'
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(30), index=True, unique=True)